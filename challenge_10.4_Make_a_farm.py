class Animal:
    def __init__(self, name, role, legs):
        self.role = role
        self.name = name
        self.legs = legs

    def welcome(self):
        print(f"This is {self.name}, his role on this farm is {self.role}.\nHis number of legs is {self.legs}.")


class Cat(Animal):
    def __init__(self, name, role, num_life):
        super().__init__(name, role, legs=3)
        self.welcome()
        self.num_life = num_life

    def miau(self):
        print(f"He has only 3 legs because of the accident. The {self.role} {self.name} "
              f"proved to everyone that it is true that cats have {self.num_life} number of lives.")


class Rooster(Animal):
    def __init__(self, name, role, speak):
        super().__init__(name, role, legs=2)
        self.welcome()
        self.speak = speak

    def koko(self):
        print(f"{self.name} is very noisy. His {self.speak} will wake up anyone.")


class Cow(Animal):
    def __init__(self, name, role,  lit_of_milk):
        super().__init__(name, role, legs=4)
        self.welcome()
        self.lit_of_milk = lit_of_milk

    def milk(self):
        print(f"{self.name} recently gave us {self.lit_of_milk} liters of milk.")


melman = Cat("Cat", "Melman", 9)
melman.miau()
print("")
rooster = Rooster("Wyjec", "Rooster", "Kuuku-Ryyyku")
rooster.koko()
print("")
klara = Cow("Klara", "Cow", 20)
klara.milk()