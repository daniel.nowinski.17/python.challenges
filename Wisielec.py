import sys

wisielec = ['''
   +---+
       |
       |
       |
      ===''', '''
   +---+
   O   |
       |
       |
       ===''', '''
   +---+
   O   |
   |   |
       |
      ===''', '''
   +---+
   O   |
  /|   |
       |
      ===''', '''
   +---+
   O   |
  /|\  |
       |
      ===''', '''
   +---+
   O   |
  /|\  |
  /    |
      ===''', '''
   +---+
   O   |
  /|\  |
  / \  |
      ===''']

print('Na tym możesz zawisnąć: ')
print(wisielec[0])
n = 0 #wartość początkowa wisielca



no_of_tries = 6
word = "iphone"
used_letters = []

user_word = []

def find_indexes(word, letter):
    indexes = []

    for index, letter_in_word in enumerate(word):
        if letter == letter_in_word:
            indexes.append(index)
    return indexes

def show_state_of_game():
    print()
    print(user_word)
    print(f"Pozostało Ci {no_of_tries} prób")
    print(f"Użyte litery to: {used_letters}")
    print()



for _ in word:
    user_word.append("_")

while True:
    letter = input("Wprowadź literę: ")
    used_letters.append(letter)

    found_indexes = find_indexes(word, letter)

    if len(found_indexes) == 0:
        print("Błędna litera.")
        n = n + 1
        print(wisielec[n])
        no_of_tries -= 1




        if no_of_tries == 0:
            print("Niestety to Twój koniec :(")
            sys.exit(0)

    else:
        for index in found_indexes:
            user_word[index] = letter

        if "".join(user_word) == word:
            print("Mission complete :)")
            sys.exit(0)


    show_state_of_game()

