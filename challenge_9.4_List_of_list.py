import statistics

universities = [
['Califotnia Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]



def enrollment_stats(universities):
    sum_of_students = []
    sum_of_tuition = []
    for uni in universities:
        sum_of_students.append(uni[1])
        sum_of_tuition.append(uni[2])
    return sum_of_students, sum_of_tuition


def mean(values):
    return sum(values) / len(values)



def median(values):
    return statistics.median(values)


summary = enrollment_stats(universities)

print("\n")
print("***** *** ! " * 6)
print(f"Total students:   {sum(summary[0]):,}")
print(f"Total tuition:  $ {sum(summary[1]):,}")
print(f"\nStudent mean:     {mean(summary[0]):,.2f}")
print(f"Student median:   {median(summary[0]):,}")
print(f"\nTuition mean:   $ {mean(summary[1]):,.2f}")
print(f"Tuition median: $ {median(summary[1]):,}")
print("***** *** ! " * 6)
print("\n")

    
