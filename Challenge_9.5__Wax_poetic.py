Nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
Verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
Adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
Prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
Adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]

import random
from random import choice

def poem():
    while True:
        
        nouns1 = random.choice(Nouns)
        nouns2 = random.choice(Nouns)
        nouns3 = random.choice(Nouns)
        if nouns1 != nouns2 and nouns2 != nouns3 and nouns3 != nouns1:
            break
    while True:
        
        verbs1 = random.choice(Verbs)
        verbs2 = random.choice(Verbs)
        verbs3 = random.choice(Verbs)
        if verbs1 != verbs2 and verbs2 != verbs3 and verbs3 != verbs1:
            break
    while True:
        
        adj1 = random.choice(Adjectives)
        adj2 = random.choice(Adjectives)
        adj3 = random.choice(Adjectives)
        if adj1 != adj2 and adj2 != adj3 and adj3 != adj1:
            break
    while True:
        
        prepo1 = random.choice(Prepositions)
        prepo2 = random.choice(Prepositions)
        if prepo1 != prepo2:
            break

    adv1 = random.choice(Adverbs)


    if "aeiou".find(adj1[0]) != -1:
        word = "An"
    else:
        word = "A"



    poem = (
        f"{word} {adj1} {nouns1}\n\n"
        f"{word} {adj1} {nouns1} {verbs1} {prepo1} the {adj2} {nouns2}\n"
        f"{adj1}, the {nouns1} {verbs2}\n"
        f"the {nouns2} {verbs3} {prepo2} a {adj3} {nouns3}"
    )

    return poem

poem = poem()
print(poem)



        
