#Challenge 1 Celsius

def convert_cel_to_far(celsius):
    '''Converted celcius degrees to fahrenheits degrees'''
    fahreinheit = celsius * 9/5 + 32
    return fahreinheit

celsius_user = input("Enter a temperature in degrees C: ")
celsius_user = int(float(celsius_user))

print(f"{celsius_user} degrees C = {convert_cel_to_far(celsius_user):.2f} degrees F")


#Challenge 2 Farhreinheit

def convert_far_to_cel(fahreinheit):
    '''Converted fahreinheit degrees to celsius degrees'''
    celsius = (fahreinheit - 32) * 5/9
    return celsius

fahreinheit_user = input("Enter a temperature in degrees F: ")
fahreinheit_user = int(float(fahreinheit_user))


print(f"{fahreinheit_user} degrees F = {convert_far_to_cel(fahreinheit_user):.2f} degrees C")
