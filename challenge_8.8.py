from random import randint

def coin_flip():
    if randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


sum_of_trials = 10000
total_flips = 0

for i in range(sum_of_trials):
    if coin_flip() == "heads":
        total_flips = total_flips + 1
        while coin_flip() == "heads":
            total_flips = total_flips + 1
        total_flips = total_flips + 1


    else:
        total_flips = total_flips + 1
        while coin_flip() == "tails":
            total_flips = total_flips + 1
        total_flips = total_flips + 1




average_flips = total_flips / sum_of_trials
print(f" The averge of flips on trials is: {average_flips}")
    


    
