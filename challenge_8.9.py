from random import random

num_wins_a = 0
num_wins_b = 0

trials = 10000




for i in range(trials):
    a_votes = 0
    b_votes = 0
    if random() < 0.87:
        a_votes = a_votes + 1
    else:
        b_votes = b_votes + 1        
    if random() < 0.65:
        a_votes = a_votes + 1
    else:
        b_votes = b_votes + 1        
    if random() < 0.17:
        a_votes = a_votes + 1
    else:
        b_votes = b_votes + 1
        
    if a_votes > b_votes:
        num_wins_a = num_wins_a + 1
    else:
        num_wins_b = num_wins_b + 1
        
print(f"Kandydat a miał {(num_wins_a / trials) * 100} % szans na wygraną.")
print(f"Kandydat b miał {(num_wins_b / trials) * 100} % szans na wygraną.")
