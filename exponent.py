base = input("Enter a base: ")
exponent = input("Enter an exponent: ")
results = float(base) ** float(exponent)
print(f"{base} to the power of {exponent} = {results}")
