import random

capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas':'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}


state, capital = random.choice(list(capitals_dict.items()))


while True:
    user_input = input(f"What is The Capital of {state}? ").lower()
    if user_input == "exit":
        print(f"Answer is '{capital}'.")
        print("Goodbye!")
        break
    elif user_input == capital.lower():
        print("Correct!")
        break
    

                               
